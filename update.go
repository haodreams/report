package report

//Updater 更新数据
type Updater interface {
	//插入数据
	Update(id string, cols, row []string) (err error)
	//操作完成
	Done() (err error)
}
