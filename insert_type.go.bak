package report

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"

	"xorm.io/xorm"
)

//DBTypeInsert 插入数据
type DBTypeInsert struct {
	bInit    bool
	sess     *xorm.Session
	db       *xorm.Engine
	errCount int
	total    int
	okCount  int
	table    *Table
	kv       map[string]*Column
	count    int
}

//NewInsertCheck .
func NewInsertCheck(engine *xorm.Engine, table *Table) *DBInsert {
	di := new(DBInsert)
	di.table = table
	di.db = engine
	di.kv = map[string]*Column{}
	for _, col := range table.Columns {
		key := strings.ToLower(col.Name)
		di.kv[key] = col
	}
	return di
}

func getTitles(buf *bufio.Reader) (cols []string) {
	line, err := buf.ReadString('\n')
	if err != nil {
		return
	}
	line = strings.TrimSpace(line)
	if line == "" {
		return nil
	}

	cols = strings.Split(line, ",")
	return
}

//SetDB .
func (m *DBInsert) SetDB(engine *xorm.Engine) {
	m.db = engine
}

func (m *DBInsert) getRow(cols []string, nCols int, buf *bufio.Reader) (sql string, err error) {
	keys := make([]string, nCols)
	values := make([]string, nCols)
	idx := 0
	line, err := buf.ReadString('\n')
	if err == nil || len(line) > 0 {
		ss := strings.Split(line, ",")
		n := len(ss)
		for i := 0; i < nCols && i < n; i++ {
			ss[i] = strings.TrimSpace(ss[i])
			ss[i] = strings.Trim(ss[i], "\"")
			if col, ok := m.kv[cols[i]]; ok {
				keys[idx] = col.Name
				values[idx] = col.GetValue(ss[i])
				idx++
			}
		}
		return
	}

	data := bytes.NewBufferString("INSERT INTO " + m.table.Name + " (")
	data.WriteString(strings.Join(keys[:idx], ","))
	data.WriteString(") VALUES (")
	data.WriteString(strings.Join(values[:idx], ","))
	data.WriteString(")")
	sql = data.String()
	return
}

//InsertRow 插入数据
func (m *DBInsert) InsertRow(sql string) (err error) {
	if m.errCount > 20 {
		return
	}
	if m.sess == nil {
		m.bInit = true
		m.sess = m.db.NewSession()
	}

	if !m.bInit {
		err = m.sess.Begin()
		if err != nil {
			m.errCount++
			return
		}
	}

	_, err = m.sess.Exec(sql)
	if err != nil {
		return
	}
	m.total++
	m.okCount++
	if m.okCount >= 2000 {
		err = m.sess.Commit()
		m.okCount = 0
		m.bInit = false
	}
	return
}

//Done 操作完成
func (m *DBInsert) Done() (err error) {
	if m.sess != nil && m.bInit {
		err = m.sess.Commit()
	}
	return
}

//Insert .
func (m *DBInsert) Insert(reader io.Reader) (err error) {
	buf := bufio.NewReader(reader)
	cols := getTitles(buf)
	nCols := len(cols)
	if nCols == 0 {
		err = errors.New("没有数据")
		return
	}
	errBuf := bytes.NewBuffer(nil)
	lineNO := 0
	for {
		lineNO++
		sql, err := m.getRow(cols, nCols, buf)
		if err != nil {
			break
		}

		e := m.InsertRow(sql)
		if e != nil {
			errBuf.WriteString(fmt.Sprintf("Line: %d", lineNO))
			errBuf.WriteString(e.Error())
			errBuf.WriteString("\r\n")
		}
	}

	e := m.Done()
	if e != nil {
		errBuf.WriteString(fmt.Sprintf("Line: %d", lineNO))
		errBuf.WriteString(err.Error())
		errBuf.WriteString("\r\n")
	}

	if errBuf.Len() > 0 {
		err = errors.New(errBuf.String())
	}
	return
}
