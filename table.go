package report

import (
	"bytes"
	"fmt"
	"strings"
)

// Table 表
type Table struct {
	Title              string
	Name               string
	Primary            string    //是主键的列名
	Columns            []*Column //全部列
	Buttons            []*Button //按钮
	DefaultColumnWidth *int      //默认宽度
	HasNumber          bool      //是否有序号列
	Template           string    //使用的模板文件
	HasRefresh         bool      //是否有刷新按钮
	SQL                string    //sql 语句集合
	DefaultOrder       string    //默认排序方式
	HasSearch          bool      //是否有搜索
	HasCheckbox        bool      //是否有复选框
	HasRadio           bool      //是否有单选框
	UseWebSocket       bool      //是否使用websocket
	DataUrl            *string   //请求数据的地址
	UploadUrl          *string   //上传数据的接口地址
	DownloadUrl        *string   //下载数据的接口地址
	DeleteUrl          *string   //删除数据的接口地址
	JSCode             *string   //js 代码
	Funcs              *string   //脚本
	// HTMLCode           string    //html 代码
}

// Fix 加载后需要修复格式
func (m *Table) Fix() {
	m.HasSearch = false
	for _, col := range m.Columns {
		col.Fix()
		if col.Search {
			m.HasSearch = true
		}
	}
	m.Template = strings.TrimSpace(m.Template)
}

// GetOper 获取操作符
func (m *Table) GetOper(colName, value string) (string, interface{}) {
	for _, col := range m.Columns {
		if col.Name == colName {
			return col.GetOper(value)
		}
	}
	return "=", value
}

// GetColumn 获取某一列
func (m *Table) GetColumn(colName string) (col *Column) {
	for _, col := range m.Columns {
		if col.Name == colName {
			return col
		}
	}
	return nil
}

// ToSQL .
func (m *Table) ToSQL(where ...string) string {
	if m.SQL != "" {
		if len(where) > 0 {
			return fmt.Sprintf(m.SQL, where[0])
		}
		return m.SQL
	}
	i := 0
	cols := make([]string, len(m.Columns))
	buf := bytes.NewBufferString("SELECT ")
	for _, col := range m.Columns {
		if col.Name == "" {
			continue
		}
		if col.Name == "-" {
			continue
		}
		cols[i] = GetDialect() + col.Name + GetDialect()
		i++
	}
	buf.WriteString(strings.Join(cols[:i], ","))
	buf.WriteString(" FROM ")
	buf.WriteString(m.Name)
	buf.WriteString(" WHERE 1=1")
	if len(where) > 0 {
		buf.WriteString(fmt.Sprint(where[0]))
	}
	return buf.String()
}

// CountSQL .
func (m *Table) CountSQL(where ...string) string {
	if m.SQL != "" {
		if len(where) > 0 {
			return "SELECT count(0) FROM (" + fmt.Sprintf(m.SQL, where[0]) + ")"
		}
		return "SELECT count(0) FROM (" + m.SQL + ")"
	}
	if len(where) > 0 {
		return "SELECT count(0) FROM " + m.Name + " WHERE 1=1 " + fmt.Sprint(where[0])
	}
	return "SELECT count(0) FROM " + m.Name + " WHERE 1=1"
}

/*
//DataTable 数据表
type DataTable struct {
	ToolbarTextHead template.HTML //
	ToolbarTextTail template.HTML //
	SearchData      template.HTML //搜索变量数据
	Option          template.HTML
	ToolbarEvent    template.HTML
}

//NewDataTable 数据表
func NewDataTable(tab *Table) *DataTable {
	dt := new(DataTable)

	return dt
}

//GetDataTable .
func (m *Table) GetDataTable() *DataTable {
	return NewDataTable(m)
}

//GetRow 。
func (m *Table) GetRow(cols []string, nCols int, buf *bufio.Reader) (sql string, err error) {

	return
}
*/
