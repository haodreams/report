# Details

Date : 2021-06-21 17:25:51

Directory d:\go\src\gitee.com\haodreams\report

Total : 10 files,  1314 codes, 119 comments, 161 blanks, all 1594 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [column.go](/column.go) | Go | 131 | 23 | 16 | 170 |
| [insert.go](/insert.go) | Go | 269 | 29 | 23 | 321 |
| [reportcs/table.go](/reportcs/table.go) | Go | 455 | 51 | 72 | 578 |
| [table.go](/table.go) | Go | 107 | 10 | 14 | 131 |
| [table_test.go](/table_test.go) | Go | 19 | 1 | 3 | 23 |
| [update.go](/update.go) | Go | 5 | 3 | 2 | 10 |
| [views/public/datatable.html](/views/public/datatable.html) | HTML | 11 | 0 | 4 | 15 |
| [views/public/footer.html](/views/public/footer.html) | HTML | 2 | 0 | 0 | 2 |
| [views/public/header.html](/views/public/header.html) | HTML | 12 | 2 | 3 | 17 |
| [views/table/index.html](/views/table/index.html) | HTML | 303 | 0 | 24 | 327 |

[summary](results.md)