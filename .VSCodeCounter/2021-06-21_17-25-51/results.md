# Summary

Date : 2021-06-21 17:25:51

Directory d:\go\src\gitee.com\haodreams\report

Total : 10 files,  1314 codes, 119 comments, 161 blanks, all 1594 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Go | 6 | 986 | 117 | 130 | 1,233 |
| HTML | 4 | 328 | 2 | 31 | 361 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 10 | 1,314 | 119 | 161 | 1,594 |
| reportcs | 1 | 455 | 51 | 72 | 578 |
| views | 4 | 328 | 2 | 31 | 361 |
| views\public | 3 | 25 | 2 | 7 | 34 |
| views\table | 1 | 303 | 0 | 24 | 327 |

[details](details.md)