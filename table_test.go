package report

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"testing"
)

func TestTable(t *testing.T) {
	tab := new(Table)
	data, err := ioutil.ReadFile("basic_fan.json")
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(data, &tab)
	if err != nil {
		log.Fatalln(err)
	}
	tab.Fix()
	//fmt.Println(string(tab.HTML()))
}
