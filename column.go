package report

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

var dialect string

func SetDialect(s string) {
	dialect = s
}

func GetDialect() string {
	return dialect
}

type Button struct {
	Title string
	Func  string
}

// Column 列
type Column struct {
	Title      string  //标题
	Name       string  //数据库对应的列明 可以是聚合函数
	Type       string  //类型 double, number
	Format     string  //格式化字符串
	Width      int     //宽度
	Fixed      string  //left（固定在左）、right（固定在右）
	Search     bool    //是否可以搜索
	SearchType string  //搜索的格式 TIME,DATE,DATETIME,NUMBER,TEXT
	Checkbox   bool    //是否由复选框
	Sort       bool    //是否支持排序
	Align      string  //left（默认）、center（居中）、right
	Oper       string  //操作符
	JsCond     string  //条件
	SortMethod *string //排序的方法
	FormatFunc string
}

// Fix 加载后需要修复格式
func (m *Column) Fix() {
	m.fixTypeFormat()
}

// GetOper 获取操作符
func (m *Column) GetOper(value string) (string, interface{}) {
	if m.Oper == "" {
		return "=", value
	}
	if m.Oper == "like" {
		value = "%" + value + "%"
	}
	return m.Oper, value
}

// GetOperInfo 获取通配符信息
func (m *Column) GetOperInfo() string {
	if strings.ToUpper(m.Oper) == "LIKE" {
		return " 支持 通配符"
	}
	return ""
}

func (m *Column) fixTypeFormat() {
	m.Align = strings.TrimSpace(m.Align)
	if m.Align == "" {
		m.Align = "center"
	}
	if m.JsCond != "" {
		m.JsCond = strings.TrimSpace(m.JsCond)
	}
	m.Type = strings.ToUpper(m.Type)
	m.Oper = strings.TrimSpace(m.Oper)
	//m.Oper = strings.ToUpper(m.Oper)
	switch m.Type {
	case "DATE":
		if m.Format == "" {
			m.Format = "YYYY/MM/DD"
		}
		if m.Width == 0 {
			m.Width = 120
		}
	case "DATETIME":
		if m.Format == "" {
			m.Format = "YYYY/MM/DD HH:mm:ss"
		}
		if m.Width == 0 {
			m.Width = 220
		}
	case "TIME":
		if m.Format == "" {
			m.Format = "HH:mm:ss"
		}
		if m.Width == 0 {
			m.Width = 100
		}
	case "FLOAT", "FLOAT64", "DOUBLE":
		if m.Format == "" {
			m.Format = "2"
		}
	}
}

// GetValue 转换值
func (m *Column) GetValue(value string) string {
	switch m.Type {
	case "STRING", "DATE", "TIME", "DATETIME":
		return "'" + value + "'"
	}

	return value
}

// GetTypeValue 获取带类型的值
func (m *Column) GetTypeValue(value string) (val interface{}, err error) {
	switch m.Type {
	case "STRING", "DATE", "TIME", "DATETIME":
		return value, nil
	case "INT", "INT8", "INT16", "INT32", "INT64":
		return strconv.Atoi(value)
	case "UINT", "UINT8", "UINT16", "UINT32", "UINT64":
		return strconv.ParseUint(value, 10, 64)
	case "FLOAT32", "FLOAT64", "FLOAT", "DOUBLE":
		return strconv.ParseFloat(value, 64)
	}
	return nil, errors.New("not have this type")
}

/*
   //格式化时间
   function formatTime(value, row, field, format) {
       return fecha.format(new Date(value * 1000), format) ;
   }

	//格式化时间
   function formatValue(value, row, field, dig) {
	   if(value == null){
		   return '-';
	   }
       return value.toFixed(dig)
   }
*/

// HTML 生成html
func (m *Column) HTML(defaultWidth int) string {
	//  { field: 'Value', title: '实时值', width: 150, templet: function (d) { return formatValue(d.Value, d, 'Value'); } },
	buf := bytes.NewBuffer(nil)
	if m.FormatFunc != "" {
		buf.WriteString("{field:'-'")
	} else {
		buf.WriteString("{field:'" + m.Name + "'")
	}

	if m.Title != "" {
		buf.WriteString(", title:'" + m.Title + "'")
	}
	if m.Sort {
		buf.WriteString(", sort:true")
	}

	if m.Align != "" {
		buf.WriteString(fmt.Sprintf(", align:'%s'", m.Align))
	}
	if m.Width > 0 {
		buf.WriteString(", width:" + fmt.Sprint(m.Width))
	} else if defaultWidth > 0 {
		buf.WriteString(", width:" + fmt.Sprint(defaultWidth))
	}

	if m.FormatFunc != "" {
		buf.WriteString(fmt.Sprintf(", templet: function (d) { return %s(d.%s, d, '%s'); }", m.FormatFunc, m.Name, m.Name))
	} else {
		switch m.Type {
		case "DATE", "DATETIME", "TIME":
			buf.WriteString(fmt.Sprintf(", templet: function (d) { return formatTime(d.%s, d, '%s', '%s'); }", m.Name, m.Name, m.Format))
		case "FLOAT", "FLOAT64", "DOUBLE":
			buf.WriteString(fmt.Sprintf(", templet: function (d) { return formatValue(d.%s, d, '%s', %s); }", m.Name, m.Name, m.Format))
		}
	}

	buf.WriteString("}\n")
	return buf.String()
}
